


# ------------------------------------------ IMPORTS -------------------------------------- #

# Import the .NET class library
import  clr 
clr.AddReference('System')

# Import python sys module 
import sys 

# import os module 
import os 

# Import C compatible List and String 
from System import String
from System.Collections.Generic import List

# Add needed dll references 
sys.path.append(os.environ['LIGHTFIELD_ROOT'])
sys.path.append(os.environ['LIGHTFIELD_ROOT']+"\\AddInViews")

clr.AddReference('PrincetonInstruments.LightFieldViewV5')
clr.AddReference('PrincetonInstruments.LightField.AutomationV5')
clr.AddReference('PrincetonInstruments.LightFieldAddInSupportServices')

# PI Imports 
import PrincetonInstruments.LightField.AddIns as AddIns
from PrincetonInstruments.LightField.Automation import Automation
from PrincetonInstruments.LightField.AddIns import CameraSettings
from PrincetonInstruments.LightField.AddIns import ExperimentSettings


# Own Imports
import NecessaryFunctions as nf

#-------------------------------- CONFIGURATION PARAMETERS --------------------------- # 

ExperimentFile = 'C:/Users/niel/Documents/LightField/Experiments/TestExperiment.lfe'

# --------------------------------- MAIN --------------------------------------------- # 

if __name__ == "__main__":

    print('\nLoading Light Field ..... \n')

    # This creates an instance of LightField Aplication. (True for visible)
    # The 2nd parameter forces LF to load with no experiment. 

    # In task manager, this opens AddInProcess.exe Note that if this AddIn is in operation 
    # no other lightfield window will be able to load the camera and spectrometer.

    auto = Automation(True, List[String]())

    # Now we load our experiment set up. 

    print('Loading Experiment ...')
    experiment = auto.LightFieldApplication.Experiment
    experiment.Load(ExperimentFile)
    print('Experiment Loaded. \n')

    # Lets check that everything loaded properly. 
    if nf.LF_Functions.device_found(experiment) == True: 
        for device in experiment.ExperimentDevices:
            print(String.Format(
                '\t{0} {1}',
                device.Model,
                device.SerialNumber))
        print('\n')
            
    else: 
        print("No experiment devices found! ")
        sys.exit()

    # Now We are ready to start with the Experimental Data adquisition 
    print('\n Starting Data Adquisition of the day \n')

    # This tells the program where to save the data of the day.
    print('Setting up Folders ... ')
    dataPath = nf.AutoDataAdquisition.FolderSetUp()
    experiment.SetValue(ExperimentSettings.FileNameGenerationDirectory, dataPath)

    print('Starting Live loop .... \n')
    nf.AutoDataAdquisition.liveLoop(experiment)






print('Light Field Closing ... ')



print('Closed ? ')

