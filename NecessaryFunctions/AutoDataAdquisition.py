# ------------------------- INPUTS --------------------------------------- # 

import time
import os


# -------------------------- PATHS --------------------------------------- # 


DataAdquisitionFolder = 'C:/Users/niel/Desktop/LightField_Stuff/TemporaryOutput/'

# ------------------------- FUNCTIONS ------------------------------------ #

def FolderSetUp(): 
    # ---- Check if there is a data folder for today --- # 
    t = time.localtime()
    current_time = time.strftime("%D", t)
    day_string = current_time.split("/")[0] + "_" + current_time.split("/")[1] + "_"+current_time.split("/")[2]

    FolderFlag = os.path.isdir(DataAdquisitionFolder + day_string)

    if FolderFlag: 
        data_Path = DataAdquisitionFolder + day_string
    else: 
        os.mkdir(DataAdquisitionFolder + day_string)
        data_Path = DataAdquisitionFolder + day_string
        
    return data_Path




def liveLoop(experiment): 

    # Create a Folder for the day: 
    print('asd')