
from PrincetonInstruments.LightField.AddIns import DeviceType

def device_found(experiment):
    # Find Connected device
    for device in experiment.ExperimentDevices: 
        if (device.Type == DeviceType.Camera): 
            return True
        
